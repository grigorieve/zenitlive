﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZenitLive
{
    public class Coefficients
    {
        public string Title { get; set; }
        public string Stage { get; set; }
        public string Market { get; set; }
        public decimal Coefficient1 { get; set; }
        public decimal Coefficient2 { get; set; }
    }
}
