﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZenitLive
{
    public class Match
    {
        public string TitleTournament { get; set; }
        public DateTime StartTime { get; set; }
        public string Player1Name { get; set; }
        //public string Player1_2Name { get; set; }
        public string Player2Name { get; set; }
        //public string Player2_2Name { get; set; }
        public string CurrentScore { get; set; }

        public List<Coefficients> coefficients = new List<Coefficients>();
    }
}
