﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZenitLive
{
    static class Pars
    {
        /// <summary>
        /// Основной метод начала парса
        /// </summary>
        public static void Start()
        {
            DateTime start = DateTime.Now;

            ChromeOptions options = new ChromeOptions();
            //options.AddArguments("headless");

            IWebDriver browser = new OpenQA.Selenium.Chrome.ChromeDriver(options);
            WebDriverWait wait = new WebDriverWait(browser, TimeSpan.FromSeconds(10));

            browser.Manage().Window.Maximize();

            browser.Navigate().GoToUrl("https://betcity.ru/en/live/tennis");
            wait.Until(driver => driver.FindElements(By.CssSelector(".line-event__dops-toggle")).Count > 0);
            Thread.Sleep(200);

            Console.WriteLine(DateTime.Now - start);
            start = DateTime.Now;


            Console.WriteLine(DateTime.Now - start);
            start = DateTime.Now;

            for (int c = 0; c < 10000; c++)
            {
                OpenDrop(browser);

                List<Match> matches = new List<Match>();

                List<IWebElement> tournamentsElement = browser.FindElements(By.CssSelector(".line__champ:not([hidden])")).ToList();
                List<IWebElement> matchesElement;
                List<IWebElement> playersElement;
                List<IWebElement> mainLineElement;

                for (int i = 0; i < tournamentsElement.Count; i++)
                {
                    try
                    {
                        matchesElement = tournamentsElement[i].FindElements(By.CssSelector("app-event-unit")).ToList();
                    }
                    catch (Exception)
                    {
                        continue;
                    }


                    for (int j = 0; j < matchesElement.Count; j++)
                    {
                        matches.Add(new Match());
                        try
                        {
                            matches[matches.Count - 1].TitleTournament = CheckElementText(tournamentsElement[i].FindElement(By.CssSelector(".line-champ__header-name")));
                            matches[matches.Count - 1].StartTime = Convert.ToDateTime(matchesElement[j].FindElement(By.CssSelector(".line-event__time-static")).Text);
                            playersElement = matchesElement[j].FindElements(By.CssSelector(".line-event__name-teams b")).ToList();
                            matches[matches.Count - 1].Player1Name = CheckElementText(playersElement[0]);
                            matches[matches.Count - 1].Player2Name = CheckElementText(playersElement[1]);
                            matches[matches.Count - 1].CurrentScore = ConvertScore(matchesElement[j].FindElement(By.CssSelector(".line-event__score-value")));
                            // Здесь можно добавить проверку на состояние текущего счета, если что то отслеживаем.
                        }
                        catch (Exception)
                        {
                            continue;
                        }

                        mainLineElement = matchesElement[j].FindElements(By.CssSelector(".line-event__main-bets-button")).ToList();
                        if (mainLineElement.Count > 0)
                        {
                            matches[matches.Count - 1].coefficients.AddRange(MainLine(mainLineElement));
                        }

                        // Отключил, пока не знаю что делать с исчезающими строчками...
                        matches[matches.Count - 1].coefficients.AddRange(OtherCoef(matchesElement[j]));



                    }

                    Console.Write("* ");
                }

                Console.WriteLine(DateTime.Now - start);
                start = DateTime.Now;

                AddInDb(matches);

            }

            browser.Quit();

            Console.WriteLine(DateTime.Now - start);
        }

        /// <summary>
        /// Преобразовывает строку в счет, добавляет инфу чья подача
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        static string ConvertScore(IWebElement element)
        {
            string s;

            s = CheckElementText(element);
            s = s.Replace("\r\n", " ");

            List<IWebElement> serve = element.FindElements(By.CssSelector(".supply-icon")).ToList();
            List<IWebElement> serve2 = element.FindElements(By.CssSelector(".supply-icon_2")).ToList();

            // Дурдом, но работает
            if (serve[0].Location == serve2[0].Location)
            {
                s += " Serve 1";
            }
            else if (serve[1].Location == serve2[0].Location)
            {
                s += " Serve 2";
            }

            return s;
        }

        static string CheckElementText(IWebElement element)
        {
            try
            {
                return element.Text;
            }
            catch (Exception)
            {
                return "error";
            }
        }

        static decimal CheckElementDecimal(IWebElement element)
        {
            try
            {
                return Convert.ToDecimal(element.Text);
            }
            catch (Exception)
            {
                return 0M;
            }
        }

        /// <summary>
        /// Добавляет все спаршенные данные в БД SQLite
        /// </summary>
        /// <param name="matches"></param>
        static void AddInDb(List<Match> matches)
        {
            string connectionString = @"Data Source = LiveCoef.db; Version = 3; FailIfMissing = True; Foreign Keys = True";

            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                conn.Open();

                for (int i = 0; i < matches.Count; i++)
                {
                    for (int j = 0; j < matches[i].coefficients.Count; j++)
                    {
                        using (SQLiteCommand cmd = new SQLiteCommand(conn))
                        {
                            cmd.CommandText = @"INSERT INTO Coefs(
                        TitleTournament,
                        StartTime,
                        Player1Name,
                        Player2Name,
                        CurrentScore,
                        Title,
                        Stage,
                        Market,
                        Coefficient1,
                        Coefficient2)

                        VALUES

                        (
                        @TitleTournament,
                        @StartTime,
                        @Player1Name,
                        @Player2Name,
                        @CurrentScore,
                        @Title,
                        @Stage,
                        @Market,
                        @Coefficient1,
                        @Coefficient2
                        )";

                            cmd.Prepare();

                            cmd.Parameters.AddWithValue("@TitleTournament", matches[i].TitleTournament);
                            cmd.Parameters.AddWithValue("@StartTime", matches[i].StartTime);
                            cmd.Parameters.AddWithValue("@Player1Name", matches[i].Player1Name);
                            cmd.Parameters.AddWithValue("@Player2Name", matches[i].Player2Name);
                            cmd.Parameters.AddWithValue("@CurrentScore", matches[i].CurrentScore);
                            cmd.Parameters.AddWithValue("@Title", matches[i].coefficients[j].Title);
                            cmd.Parameters.AddWithValue("@Stage", matches[i].coefficients[j].Stage);
                            cmd.Parameters.AddWithValue("@Market", matches[i].coefficients[j].Market);
                            cmd.Parameters.AddWithValue("@Coefficient1", matches[i].coefficients[j].Coefficient1);
                            cmd.Parameters.AddWithValue("@Coefficient2", matches[i].coefficients[j].Coefficient2);

                            try
                            {
                                cmd.ExecuteNonQuery();
                            }
                            catch (SQLiteException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                        }

                    }
                }

                conn.Close();

            }
        }

        /// <summary>
        /// Парсит данные с открытых вкладок дополнительных коэффициентов
        /// </summary>
        /// <param name="matchElement"></param>
        /// <returns></returns>
        static List<Coefficients> OtherCoef(IWebElement matchElement)
        {
            decimal n;
            string s;

            List<Coefficients> coef = new List<Coefficients>();

            List<IWebElement> marketsElement;
            marketsElement = matchElement.FindElements(By.CssSelector(".dops-item")).ToList();

            for (int i = 0; i < marketsElement.Count; i++)
            {
                List<IWebElement> lines;
                try
                {
                    lines = marketsElement[i].FindElements(By.CssSelector(".dops-item-row__section")).ToList();
                }
                catch (Exception)
                {
                    continue;
                }

                for (int j = 0; j < lines.Count; j++)
                {
                    try
                    {
                        s = marketsElement[i].FindElement(By.CssSelector(".dops-item__title")).Text;

                        if (s == "HANDICAP")
                        {
                            coef.Add(new Coefficients());
                            coef[coef.Count - 1].Title = "AH";
                            coef[coef.Count - 1].Stage = "Match";
                            coef[coef.Count - 1].Market = CheckElementText(lines[j].FindElement(By.CssSelector(".dops-item-row__block-left")));

                            List<IWebElement> lc = marketsElement[i].FindElements(By.CssSelector(".dops-item-row__block-right")).ToList();
                            coef[coef.Count - 1].Coefficient1 = CheckElementDecimal(lc[0]);
                            coef[coef.Count - 1].Coefficient2 = CheckElementDecimal(lc[1]);
                        }
                        else if (s.Contains("TO WIN A GAME"))
                        {
                            coef.Add(new Coefficients());
                            coef[coef.Count - 1].Title = "Home / Away";
                            coef[coef.Count - 1].Stage = CheckElementText(lines[j].FindElement(By.CssSelector(".dops-item-row__block-content")));
                            coef[coef.Count - 1].Market = "moneyline";

                            List<IWebElement> lc = marketsElement[i].FindElements(By.CssSelector(".dops-item-row__block-right")).ToList();
                            coef[coef.Count - 1].Coefficient1 = CheckElementDecimal(lc[0]);
                            coef[coef.Count - 1].Coefficient2 = CheckElementDecimal(lc[1]);
                        }

                        // TO WIN POINT
                        // TOTAL SETS
                        // SETS HANDICAP
                        // HANDICAP
                        // SCORE (SET HANDICAP), 
                        // 
                        // TOTAL
                        // SET RESULT (тут тотал в сете)
                        // SET RESULT (HANDICAP)
                        // SET SCORE
                        // SCORE
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("vanish");
                    }


                }

                //Console.WriteLine(marketsElement[i].FindElement(By.CssSelector(".dops-item__title")).Text);
            }

            return coef;
        }

        /// <summary>
        /// Парсит данные с основной "линии"
        /// </summary>
        /// <param name="mainLineElement"></param>
        /// <returns></returns>
        static List<Coefficients> MainLine(List<IWebElement> mainLineElement)
        {
            decimal n;

            List<Coefficients> mainLine = new List<Coefficients>();

            mainLine.Add(new Coefficients());
            mainLine[mainLine.Count - 1].Title = "Home / Away";
            mainLine[mainLine.Count - 1].Stage = "Match";
            mainLine[mainLine.Count - 1].Market = "moneyline";
            mainLine[mainLine.Count - 1].Coefficient1 = CheckElementDecimal(mainLineElement[0]);
            mainLine[mainLine.Count - 1].Coefficient2 = CheckElementDecimal(mainLineElement[2]);

            mainLine.Add(new Coefficients());
            mainLine[mainLine.Count - 1].Title = "AH";
            mainLine[mainLine.Count - 1].Stage = "Match";
            mainLine[mainLine.Count - 1].Market = "Games " + CheckElementText(mainLineElement[3]);
            mainLine[mainLine.Count - 1].Coefficient1 = CheckElementDecimal(mainLineElement[4]);
            mainLine[mainLine.Count - 1].Coefficient2 = CheckElementDecimal(mainLineElement[6]);

            mainLine.Add(new Coefficients());
            mainLine[mainLine.Count - 1].Title = "O/U";
            mainLine[mainLine.Count - 1].Stage = "Match";
            mainLine[mainLine.Count - 1].Market = "Games " + CheckElementText(mainLineElement[7]);
            mainLine[mainLine.Count - 1].Coefficient1 = CheckElementDecimal(mainLineElement[8]);
            mainLine[mainLine.Count - 1].Coefficient2 = CheckElementDecimal(mainLineElement[9]);

            return mainLine;
        }

        /// <summary>
        /// Открывает все вкладки дополнительных коэффициентов
        /// </summary>
        /// <param name="browser"></param>
        static void OpenDrop(IWebDriver browser)
        {
            List<IWebElement> elements = browser.FindElements(By.CssSelector(".line-event__dops-toggle")).ToList();

            //for (int i = 0; i < elements.Count; i++) // Как должно быть
            for (int i = 0; i < 30; i++) // Чтобы не тратить время при тестировании
            {
                try
                {
                    if (elements[i].Text.Contains("+"))
                        elements[i].Click();
                }
                catch (Exception)
                {
                    Console.WriteLine("no click");
                }
            }
        }

    }
}

